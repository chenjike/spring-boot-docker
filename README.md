## Docker + Spring Boot: 快速搭建和部署Java Web应用
这里安装docker步骤演示
## 2、安装Docker

下载地址：https://download.docker.com/linux/static/stable/x86_64/docker-19.03.9.tgz

这里采用二进制安装，用yum安装也一样。

### 2.1 解压二进制包
tar zxvf docker-19.03.9.tgz
mv docker/* /usr/bin

### 2.2 systemd管理docker

```
cat > /usr/lib/systemd/system/docker.service << EOF
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network-online.target firewalld.service
Wants=network-online.target
[Service]
Type=notify
ExecStart=/usr/bin/dockerd
ExecReload=/bin/kill -s HUP $MAINPID
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
TimeoutStartSec=0
Delegate=yes
KillMode=process
Restart=on-failure
StartLimitBurst=3
StartLimitInterval=60s
[Install]
WantedBy=multi-user.target
EOF
```


### 2.3 创建配置文件

```
mkdir /etc/docker
cat > /etc/docker/daemon.json << EOF
{
“registry-mirrors”: [“https://b9pmyelo.mirror.aliyuncs.com“]
}
EOF
```


### 2.4 启动并设置开机启动

```
* systemctl daemon-reload 
* systemctl start docker #启动docker容器
* systemctl enable docker #启用开机自启
```


**0、你需要：**（如已打好java的jar包可以忽略，有安装docker即可）

* JDK 1.8 : java -version
* Maven 3.0+ : mvn -v
* Git : git --version
* Source Code : https://gitee.com/chenjike/spring-boot-docker
* Docker : docker version
    * docker-machine ls
    * docker-machine start
    * docker-machine env
    * eval $(docker-machine env)

**1、Maven编译工程**

下载源码到本地，进入工程目录，执行maven编译

    git clone https://gitee.com/chenjike/spring-boot-docker/.git
    cd spring-boot-docker
    tree

```
项目结构：
├── README.md
├── pom.xml
└── src
    ├── main
    │   ├── docker
    │   │   ├── Dockerfile
    │   │   └── gs-spring-boot-docker-0.1.0.jar
    │   ├── java
    │   │   └── hello
    │   │       └── Application.java
    │   └── resources
    │       └── application.yml
    └── test
        └── java
            └── hello
                └── HelloWorldConfigurationTests.java
```
   打jar包
    mvn clean package -Dmaven.test.skip=true

**2、测试Jar包执行**

执行生成的jar包，运行spring boot应用

    java -jar target/gs-spring-boot-docker-0.1.0.jar

**3、验证本地运行是否可以访问成功**

* 命令行下访问：curl http://127.0.0.1:8080/
* 浏览器中访问：http://127.0.0.1:8080/

---

**4、编写Dockerfile文件**

进入到源码的docker目录下，编写Dockerfile文件

    mkdir spring-boot-docker
    cd spring-boot-docker
    拷贝编译好的gs-spring-boot-docker-0.1.0.jar到当前目录，和Dockerfile放在同一目录

    # 编写Dockerfile文件
    FROM java:8-jdk-alpine
    LABEL maintainer chenjike/www.sieiot.com
    RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
    ADD gs-spring-boot-docker-0.1.0.jar app.jar
    CMD ["java","-jar","/app.jar"]

**5、构建Dockerfile**

    docker build -t cloudcomb/springbootdocker:1.0 .
    注：-t 后面是镜像名字:版本号，后面有个空格然后点号

**6、查看构建的镜像**

    docker images

    REPOSITORY                       TAG                 IMAGE ID            CREATED              SIZE
    cloudcomb/springbootdocker       1.0                 c5a57ce057e7        About a minute ago   180.8 MB

**7、运行docker容器**

    docker run -d -p 8081:8080 --name gs cloudcomb/springbootdocker:1.0
    docker ps
    注：-d 后台运行
        -p 宿主机端口：容器端口
        --name 容器名字
        --restart 是否重启（可选）
        cloudcomb/springbootdocker:1.0 是镜像包

**8、验证Docker容器运行是否可以访问成功**

* 新建一个命令行tag：command+T
* 命令行下访问：curl http://192.168.99.100:8081
* 浏览器中访问：http://192.168.99.100:8081
